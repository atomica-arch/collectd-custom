# Maintainer: Jason R. McNeil <jason@jasonrm.net>
# Contributor: Gaetan Bisson <bisson@archlinux.org>
# Contributor: Gerhard Brauer <gerhard.brauer@web.de>

_pkgname=collectd
pkgname=collectd-custom
pkgver=5.5.0
pkgrel=1
pkgdesc='Daemon which collects system performance statistics periodically'
url='http://collectd.org/'
arch=('i686' 'x86_64')
license=('GPL')

provides=('collectd')
conflicts=('collectd')

optdepends=(
    'curl: apache, ascent, curl, nginx, and write_http plugins'
    'hiredis: redis plugin'
    'iproute2: netlink plugin'
    'libatasmart: smart plugin'
    'libdbi: dbi plugin'
    'libesmtp: notify_email plugin'
    'libgcrypt: encryption and authentication for network plugin'
    'libmariadbclient: mysql plugin'
    'libmemcached: memcachec plugin'
    'libnotify: notify_desktop plugin'
    'liboping: ping plugin'
    'libpcap: dns plugin'
    'libvirt: libvirt plugin'
    'libxml2: ascent and libvirt plugins'
    'lm_sensors: lm_sensors and sensors plugins'
    'net-snmp: snmp plugin'
    'perl: perl plugin'
    'postgresql-libs: postgresql plugin'
    'python2: python plugin'
    'rrdtool: rrdtool and rrdcached plugins'
    'varnish: varnish plugin'
    'yajl: curl_json plugin'
)

makedepends=(
    'curl'
    'hiredis'
    'iproute2'
    'libatasmart'
    'libdbi'
    'libesmtp'
    'libgcrypt'
    'libmariadbclient'
    'libmemcached'
    'libnotify'
    'liboping'
    'libpcap'
    'libvirt'
    'libxml2'
    'lm_sensors'
    'net-snmp'
    'postgresql-libs'
    'python2'
    'rrdtool'
    'varnish'
    'yajl'
)

depends=(
    'libltdl'
    'iptables'
)

source=(
    "${url}files/${_pkgname}-${pkgver}.tar.bz2"
    'service'
)
sha1sums=(
    'ef97838e249814ad96a2edc0410326efbb4f3d08'
    '04f676d0b76c34df0bbf94629813e035b1febe04'
)

backup=('etc/collectd.conf')

prepare() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	sed 's/-Werror//g' -i *.ac */*.{am,in} */*/*.{am,in}
	autoreconf
}

build() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	export MAKEFLAGS='-j1'
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--sbindir=/usr/bin \
		--with-python=/usr/bin/python2 \
		--with-perl-bindings='INSTALLDIRS=vendor'
	make all
}

package() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	make DESTDIR="${pkgdir}" install
	rmdir "${pkgdir}/var/run" # FS#30201
	install -Dm644 ../service "${pkgdir}"/usr/lib/systemd/system/collectd.service
	install -Dm644 contrib/collectd2html.pl "${pkgdir}"/usr/share/collectd/collectd2html.pl
}
